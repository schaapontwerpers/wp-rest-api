<?php
/*
Plugin Name:  WP Rest API
Plugin URI:   https://packagist.org/packages/schaapdigitalcreatives/wp-rest-api
Description:  Custom endpoints and rest fields for WP Blocks.
Version:      1.14.11
Author:       Schaap Digital Creatives
Author URI:   https://schaapontwerpers.nl/
License:      GNU General Public License
*/

new WordPressRestApi\Init();
