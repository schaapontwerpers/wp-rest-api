<?php

namespace WordPressRestApi\Endpoints;

use WordPressRestApi\Helpers\Populate\Post;
use WordPressClassHelpers\Register\RestRoute;
use WordPressRestApi\Helpers\Modify\Params;
use WP_Post;
use WP_Query;
use WP_REST_Request;
use WP_REST_Response;
use WP_REST_Server;
use WP_Term_Query;

class Search extends RestRoute
{
    private $facets = [];

    private $hits = [];

    private $params;

    private $postIds = [];

    private $totalPages = 1;

    private $totalPosts = 0;

    protected $methods = WP_REST_Server::READABLE;

    protected function setRoute()
    {
        $this->route = 'search';
    }

    /**
     * Build the callback
     */
    public function getCallback(WP_REST_Request $request)
    {
        $this->params = new Params($request);

        $this->setIds();
        $this->doQuery();

        $response = new WP_REST_Response(
            [
                'hits' => $this->hits,
                'facets' => $this->facets,
                'nbHits' => $this->totalPosts,
                'nbPages' => $this->totalPages,
            ]
        );

        return rest_ensure_response($response);
    }

    /**
     * Get the permission callback
     */
    public function getPermissionCallback(): bool
    {
        return true;
    }

    private function setIds()
    {
        $sortingParams = $this->params->getSortingParams();

        $args = [
            'fields' => 'ids',
            'post_type' => 'any',
            'orderby' => $sortingParams['orderby'],
            'order' => $sortingParams['order'],
            'posts_per_page' => -1,
            'meta_query' => [
                'relation' => 'OR',
                [
                    'key' => 'exclude_post',
                    'compare' => 'NOT EXISTS' // this should work...
                ],
                [
                    'key' => 'exclude_post',
                    'value' => 0
                ],
            ]
        ];

        $this->params->setArgs($args);
        $this->params->setPostType();
        $this->params->setTaxQuery();

        $query = new WP_Query($this->params->args);

        $this->postIds = $query->posts;
    }

    /**
     * Set the search results based on the provided search string.
     */
    private function doQuery()
    {
        $perPage = $this->params->getParam("per_page");
        $page = $this->params->getParam("page");

        $args = [
            'orderby' => 'post__in',
            'post_type' => 'any',
            'posts_per_page' => $perPage ?: 12,
            'paged' => (int) $page ?: 1,
            'post__in' => count($this->postIds) > 0 ? $this->postIds : [0],
        ];

        $query = new WP_Query($args);

        $this->hits = $query->have_posts() ?
            $this->filterPosts($query->posts) :
            [];
        $this->totalPosts = $query->found_posts;
        $this->totalPages = $query->max_num_pages;

        $this->setFacets();
    }

    /**
     * Retrieve Post objects.
     */
    private function filterPosts($posts): array
    {
        return array_map(function (WP_Post $post) {
            $postHelper = new Post($post);

            return $postHelper->filterPost();
        }, $posts);
    }

    private function setFacets()
    {
        $paramFacets = $this->params->getParam('facets');

        if ($paramFacets) {
            $facets = explode(',', $paramFacets);

            foreach ($facets as $facet) {
                $taxonomy = str_replace(
                    ['terms.', '.names'],
                    '',
                    $facet
                );

                $this->facets[$facet] = $this->getTaxonomyTerms($taxonomy);
            }
        }
    }

    private function getTaxonomyTerms(
        string $taxonomySlug,
    ): array {
        $query = new WP_Term_Query(
            [
                'taxonomy' => $taxonomySlug,
                'fields' => 'all',
                'exclude' => 1,
            ]
        );
        $terms = $query->get_terms();
        $termsWithCount = [];

        if (is_array($terms) && count($terms) > 0) {
            foreach ($terms as $term) {
                $args = [
                    'post_type' => 'any',
                    'post__in' => count($this->postIds) > 0 ?
                        $this->postIds :
                        [0],
                    'fields' => 'ids',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'tax_query' => [
                        [
                            'taxonomy' => $taxonomySlug,
                            'terms' => $term->term_id,
                        ]
                    ]
                ];

                $termQuery = new WP_Query($args);

                if ($termQuery->found_posts > 0) {
                    $termsWithCount[html_entity_decode($term->name)] = $termQuery->found_posts;
                }
            }
        }

        arsort($termsWithCount);

        return $termsWithCount;
    }
}
