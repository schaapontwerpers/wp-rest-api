<?php

namespace WordPressRestApi\Endpoints;

use WP_Error;
use WP_Query;
use WP_REST_SERVER;
use WP_REST_Request;
use WP_REST_Response;
use EasyDOM\DOMDocument;
use libphonenumber\PhoneNumberUtil;
use WordPressClassHelpers\Register\RestRoute;

class Forms extends RestRoute
{
    private $DOM;

    private $emails = '';

    private $errorResponse;

    private $ignoreBlocks = [
        'jabbado/form-fieldset',
        'jabbado/form-confirmation',
        'jabbado/form-radio-item',
        'jabbado/form-submit',
    ];

    private $files = [];

    private $formBlock;

    private $formInnerBlocks;

    private $formId;

    private $headers;

    private $inputs = [];

    private $invalidValues = [];

    private $requiredFields = [];

    private $tableHTML;

    private $url;

    private $bodyParams;

    private $fileParams;

    protected $methods = WP_REST_SERVER::CREATABLE;

    protected function setRoute()
    {
        $this->route = 'forms';
        $this->args = [
            'id' => [
                'required' => true,
                'validate_callback' => function ($param) {
                    return is_string($param);
                },
            ],
            'url' => [
                'required' => true,
                'validate_callback' => function ($param) {
                    return is_string($param);
                },
            ],
        ];
    }

    /**
     * Build the callback
     */
    public function getCallback(WP_REST_Request $request)
    {
        $response = [];

        $this->bodyParams = $request->get_body_params();
        $this->fileParams = $request->get_file_params();
        $this->formId = $request->get_param('id');
        $this->url = $request->get_param('url');

        $this->bindValuesToFormData();

        if (!$this->errorResponse && count($this->inputs) > 0) {
            $validated = $this->validate();

            if ($validated) {
                $this->buildHTML();

                $homeUrl = parse_url(home_url());
                $homeUrlParts = explode('.', $homeUrl['host']);

                $mailFromName = get_bloginfo('name');
                $mailFrom = 'noreply@' .
                    $homeUrlParts[count($homeUrlParts) - 2] .
                    '.' .
                    $homeUrlParts[count($homeUrlParts) - 1];

                $this->headers = array(
                    'From: ' . $mailFromName . ' <' . $mailFrom . '>',
                    'MIME-Version: 1.0',
                    'Content-Type: text/html; charset=UTF-8',
                );

                if (
                    (array_key_exists(
                        'sendEmail',
                        $this->formBlock['attrs']
                    ) && $this->formBlock['attrs']['sendEmail']) ||
                    !array_key_exists(
                        'sendEmail',
                        $this->formBlock['attrs']
                    )
                ) {
                    $this->sendEmail();
                }

                if (!$this->errorResponse) {
                    $customSubmitResponse = apply_filters(
                        'jabbado_form_submit',
                        $this->inputs,
                        $this->files,
                        $this->formBlock['attrs']
                    );

                    if ($customSubmitResponse instanceof WP_Error) {
                        $this->errorResponse = $customSubmitResponse;
                    }
                }

                if (
                    !$this->errorResponse &&
                    (array_key_exists(
                        'sendConfirmation',
                        $this->formBlock['attrs']
                    ) && $this->formBlock['attrs']['sendConfirmation']) ||
                    !array_key_exists(
                        'sendConfirmation',
                        $this->formBlock['attrs']
                    )
                ) {
                    $this->sendConfirmation();
                }
            }
        }

        if ($this->errorResponse && $this->errorResponse->has_errors()) {
            $response = $this->errorResponse;
        } else {
            $response = new WP_REST_Response(
                [
                    'code' => 'submit_succesful',
                    'message' => __(
                        'Het formulier is succesvol verzonden!',
                        'jabbado'
                    ),
                ],
            );
            $response->set_status(201);
        }

        return rest_ensure_response($response);
    }

    /**
     * Get the permission callback
     */
    public function getPermissionCallback(): bool
    {
        return true;
    }

    /**
     * Validate all fields
     */
    private function bindValuesToFormData()
    {
        // $frontendUrl = function_exists('get_field') ?
        //     get_field('frontend_url', 'options') :
        //     null;
        // $url = $frontendUrl ?: home_url();

        // $pagename = explode(
        //     '?',
        //     explode(
        //         '#',
        //         basename(
        //             untrailingslashit(
        //                 str_replace($url, '', $this->url)
        //             )
        //         )
        //     )[0]
        // )[0];

        $postTypes = array_values(
            get_post_types(['exclude_from_search' => false])
        );
        array_push($postTypes, 'wp_block');

        $query = new WP_Query([
            's' => '"formId":"' . $this->formId . '"',
            'posts_per_page' => 1,
            'post_type' => $postTypes,
        ]);

        if ($query->have_posts()) {
            $content = $query->posts[0]->post_content;
            $blocks = parse_blocks($content);

            $this->findBlock($blocks);

            if ($this->formBlock) {
                $this->formInnerBlocks = $this->flattenBlocks(
                    $this->formBlock['innerBlocks']
                );

                foreach ($this->formInnerBlocks as $formInnerBlock) {
                    if (str_starts_with(
                        $formInnerBlock['blockName'],
                        'jabbado/form-'
                    )) {
                        $inputId = $this->formId .
                            '-' .
                            $formInnerBlock['attrs']['inputId'];
                        $type = $this->getInputType($formInnerBlock);
                        $value = '';

                        if (array_key_exists($inputId, $this->bodyParams)) {
                            $value = $this->bodyParams[$inputId];
                        } else if (
                            array_key_exists($inputId, $this->fileParams)
                        ) {
                            $value = $this->fileParams[$inputId];
                        }

                        $input = [
                            'id' => $inputId,
                            'label' => array_key_exists(
                                'label',
                                $formInnerBlock['attrs']
                            ) ? $formInnerBlock['attrs']['label'] : '',
                            'type' => $type,
                            'value' => $this->getValue($type, $value),
                            'required' => array_key_exists(
                                'isRequired',
                                $formInnerBlock['attrs']
                            ) ? $formInnerBlock['attrs']['isRequired'] : true,
                            'externalName' => array_key_exists(
                                'externalName',
                                $formInnerBlock['attrs']
                            ) ? $formInnerBlock['attrs']['externalName'] : '',
                        ];

                        array_push($this->inputs, $input);
                    } else {
                        $value = '';

                        if (!array_key_exists(
                            'content',
                            $formInnerBlock['attrs']
                        )) {
                            $value = trim(strip_tags(
                                html_entity_decode(
                                    $formInnerBlock['innerHTML']
                                ),
                                '<a><mark><i><strong><b><br><em><span><u><sub><sup><s>'
                            ));
                        } else {
                            $value = $formInnerBlock['attrs']['content'];
                        }

                        array_push($this->inputs, [
                            'type' => str_replace(
                                'jabbado/',
                                '',
                                $formInnerBlock['blockName']
                            ),
                            'value' => $value
                        ]);
                    }
                }
            } else {
                $this->errorResponse = new WP_Error(
                    'invalid_form_id',
                    __(
                        'Het lijkt erop dat dit formulier vernieuwd is terwijl je het invulde. Ververs de pagina en probeer het opnieuw.',
                        'jabbado'
                    ),
                    [
                        'status' => 404,
                    ]
                );
            }
        } else {
            $this->errorResponse = new WP_Error(
                'invalid_form_id',
                __(
                    'Het lijkt erop dat dit formulier vernieuwd is terwijl je hem aan het invullen was. Ververs de pagina en probeer het opnieuw.',
                    'jabbado'
                ),
                [
                    'status' => 404,
                ]
            );
        }
    }

    private function flattenBlocks(array $innerBlocks): array
    {
        $blocks = [];

        foreach ($innerBlocks as $innerBlock) {
            if (!in_array($innerBlock['blockName'], $this->ignoreBlocks)) {
                $blocks[] = $innerBlock;
            }

            if ($innerBlock['blockName'] === 'jabbado/form-fieldset') {
                if (
                    array_key_exists('innerBlocks', $innerBlock) &&
                    count($innerBlock['innerBlocks']) > 0
                ) {
                    $children = $this->flattenBlocks(
                        $innerBlock['innerBlocks']
                    );

                    foreach ($children as $child) {
                        $blocks[] = $child;
                    }
                }
            }
        }

        return $blocks;
    }

    private function getValue(string $type, $value): string
    {
        $returnValue = '';

        if ($type === 'fileupload') {
            if (is_array($value) && array_key_exists('name', $value)) {
                $returnValue = $value['name'];

                if (array_key_exists('tmp_name', $value)) {
                    $this->files[$value['name']] = $value['tmp_name'];
                }
            }
        } else {
            $returnValue = $this->stripString($value);
        }

        return $returnValue;
    }

    /**
     * Strip input value
     */
    private function stripString(string $string): string
    {
        $string = strip_tags($string);
        $string = trim($string);
        $string = stripslashes($string);
        $string = htmlspecialchars($string);

        return $string;
    }

    private function findBlock(array $blocks)
    {
        foreach ($blocks as $block) {
            if ($block['blockName'] === 'jabbado/form') {
                $this->formBlock = $block;

                break;
            } elseif (
                array_key_exists('innerBlocks', $block) &&
                count($block['innerBlocks']) > 0
            ) {
                $this->findBlock($block['innerBlocks']);

                if ($this->formBlock) {
                    break;
                }
            }
        }
    }

    /**
     * Get the type of the input
     */
    private function getInputType(array $input): string
    {
        $type = '';

        if ($input['blockName'] === 'jabbado/form-input') {
            $type = array_key_exists('type', $input['attrs']) ?
                $input['attrs']['type'] :
                'text';
        } else {
            $type = str_replace('jabbado/form-', '', $input['blockName']);
        }

        return $type;
    }

    /**
     * Get the checkbox value for in the email
     */
    private function getCheckboxValue(string $value): string
    {
        $translateValue = [
            'on' => __('Checked', 'jabbado'),
            'off' => __('Unchecked', 'jabbado'),
        ];

        return $translateValue[$value !== '' ? $value : 'off'];
    }

    /**
     * Build the HTML message
     */
    private function validate(): bool
    {
        foreach ($this->inputs as $input) {
            if (
                $input['type'] !== 'heading' && $input['type'] !== 'paragraph'
            ) {
                if (
                    $input['required'] &&
                    (
                        ($input['value'] === '') ||
                        (($input['type'] === 'checkbox') &&
                            ($input['value'] === 'off'))
                    )
                ) {
                    array_push($this->requiredFields, $input['id']);
                } elseif ($input['value'] !== '') {
                    $this->validateInput(
                        $input['id'],
                        $input['value'],
                        $input['type'],
                    );
                }
            }
        }

        $errorsCount = count($this->requiredFields) +
            count($this->invalidValues);

        if ($errorsCount !== 0) {
            $this->errorResponse = new WP_Error(
                'validation_failed',
                __('Validatie van een of meer velden is mislukt.', 'jabbado'),
                [
                    'status' => 400,
                ]
            );

            if (count($this->invalidValues) > 0) {
                $this->errorResponse->add(
                    'value_invalid',
                    __('Niet alle velden zijn correct ingevuld.', 'jabbado'),
                    $this->invalidValues,
                );
            }

            if (count($this->requiredFields) > 0) {
                $this->errorResponse->add(
                    'field_required',
                    __('Niet alle verplichte velden zijn ingevuld.', 'jabbado'),
                    $this->requiredFields,
                );
            }
        }

        return $errorsCount === 0;
    }

    /**
     * Validate a single field
     */
    private function validateInput(
        string $id,
        string $value,
        string $type,
    ) {
        switch ($type) {
            case 'email':
                if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    array_push($this->invalidValues, $id);
                } else {
                    $this->emails .= (strlen($this->emails) > 0) ?
                        ',' . $value :
                        $value;
                }

                break;

            case 'tel':
                $phoneUtil = PhoneNumberUtil::getInstance();
                $phoneNumber = $phoneUtil->parse(
                    $value,
                    str_starts_with($value, '+') ? null : 'NL'
                );

                if (!$phoneUtil->isValidNumber($phoneNumber)) {
                    array_push($this->invalidValues, $id);
                }

                break;

            case 'tel':
                $phoneUtil = PhoneNumberUtil::getInstance();
                $phoneNumber = $phoneUtil->parse(
                    $value,
                    str_starts_with($value, '+') ? null : 'NL'
                );

                if (!$phoneUtil->isValidNumber($phoneNumber)) {
                    array_push($this->invalidValues, $id);
                }

                break;

            default:
                break;
        }
    }

    /**
     * Build the HTML message
     */
    private function buildHTML()
    {
        // Build DomDocument
        $this->DOM = new DOMDocument();

        // Fill table with POST data
        $table = $this->createTable();

        // Build DOM message
        $html = $this->DOM->createElement('html');
        $body = $this->DOM->createElement('body');
        $body->appendChild($table);
        $html->appendChild($body);

        // Save DOM to message
        $this->DOM->appendChild($html);

        // Filter hook for adjustability
        $this->tableHTML = apply_filters(
            'jabbado_table_html',
            $this->DOM->saveHTML($table),
            $this->inputs,
        );
    }

    /**
     * Send the form
     */
    private function createTable()
    {
        // Create table element
        $atts = array(
            'rules' => 'all',
            'cellpadding' => '0',
            'width' => '100%',
            'style' => 'max-width: 600px; margin: 0 auto;',
        );
        $table = $this->DOM->generateElement('table', $atts);
        $tbody = $this->DOM->createElement('tbody');
        $table->appendChild($tbody);

        $i = 0;

        foreach ($this->inputs as $input) {
            $value = $input['type'] === 'checkbox' ?
                $this->getCheckboxValue($input['value']) :
                $input['value'];

            if (
                $input['type'] === 'heading' ||
                $input['type'] === 'paragraph'
            ) {
                $tr = $this->createTextRow($input['value']);
            } else {
                $tr = $this->createRow(
                    $input['label'],
                    $value,
                    $i
                );
            }

            $tbody->appendChild($tr);

            $i++;
        }

        // Add referer to table
        $tr = $this->createRow(
            __('URL', 'jabbado'),
            $this->url,
            $i
        );
        $tbody->appendChild($tr);

        return $table;
    }

    /**
     * Create a row for the table
     */
    private function createTextRow(
        string $value,
    ) {
        $backgroundColor = '#fff';

        $atts = array(
            'style' =>
            'color: #000; background-color: ' . $backgroundColor . ';'
        );
        $tr = $this->DOM->generateElement('tr', $atts);

        $atts = array(
            'align' => 'left',
            'style' => 'border: 0; padding: 9px 0 9px 0;',
            'valign' => 'top',
            'colspan' => 2,
        );
        $td = $this->DOM->generateElement('td', $atts, $value);
        $tr->appendChild($td);

        return $tr;
    }

    /**
     * Create a row for the table
     */
    private function createRow(
        string $label,
        string $value,
        int $i
    ) {
        $backgroundColor = ($i % 2 == 0) ? '#fefefe' : '#fff';

        $atts = array(
            'style' =>
            'color: #000; background-color: ' . $backgroundColor . ';'
        );
        $tr = $this->DOM->generateElement('tr', $atts);

        $atts = array(
            'align' => 'left',
            'style' => 'border-color: transparent; padding: 9px 36px 9px 0;',
            'valign' => 'top'
        );
        $th = $this->DOM->generateElement('th', $atts);
        $tr->appendChild($th);

        $strong = $this->DOM->generateElement(
            'strong',
            null,
            $label
        );
        $th->appendChild($strong);

        $atts = array(
            'align' => 'left',
            'style' => 'border: 0; padding: 9px 0 9px 0; width: 60%;',
            'valign' => 'top'
        );
        $td = $this->DOM->generateElement('td', $atts, $value);
        $tr->appendChild($td);

        return $tr;
    }

    /**
     * Send the e-mail
     */
    private function sendEmail()
    {
        $to = array_key_exists('emailAddress', $this->formBlock['attrs']) ?
            $this->formBlock['attrs']['emailAddress'] :
            get_option('admin_email');
        $subject = array_key_exists(
            'emailSubject',
            $this->formBlock['attrs']
        ) ?
            $this->formBlock['attrs']['emailSubject'] :
            __('New form submission', 'jabbado');

        // Send mail
        $mail = wp_mail(
            $to,
            $subject,
            $this->tableHTML,
            $this->headers,
            array_values($this->files),
        );

        if (!$mail) {
            $this->errorResponse = new WP_Error(
                'mail_failed',
                __(
                    'Het is niet gelukt om het formulier te verzenden. Zou je het nog eens willen proberen?',
                    'jabbado'
                ),
                [
                    'status' => 500,
                ]
            );
        }
    }

    /**
     * Send the confirmation e-mail
     */
    private function sendConfirmation()
    {
        $subject = array_key_exists(
            'confirmationSubject',
            $this->formBlock['attrs']
        ) ?
            $this->formBlock['attrs']['confirmationSubject'] :
            __('Confirmation of form submission', 'jabbado');

        $text = array_key_exists(
            'confirmationText',
            $this->formBlock['attrs']
        ) ?
            $this->formBlock['attrs']['confirmationText'] :
            '';
        $confirmationText = $text !== '' ?
            $text :
            __('Thanks for filling out our form!', 'jabbado');
        $confirmationText = apply_filters(
            'jabbado_confirmation_email',
            $confirmationText,
            $this->inputs
        );

        // Send mail
        $mail = wp_mail(
            $this->emails,
            $subject,
            $confirmationText,
            $this->headers
        );

        if (!$mail) {
            $this->errorResponse = new WP_Error(
                'mail_failed',
                __(
                    'Het is niet gelukt om een bevestigingsmail te versturen naar het opgegeven e-mailadres. Maak je geen zorgen, wij hebben het formulier wel ontvangen!',
                    'jabbado'
                ),
                [
                    'status' => 500,
                ]
            );
        }
    }

    private function sendErrorMail()
    {
        $to = get_option('admin_email');
        $subject = __('Error on form submission', 'jabbado');

        // Send mail
        wp_mail(
            $to,
            $subject,
            $this->tableHTML,
            $this->headers,
        );
    }
}
