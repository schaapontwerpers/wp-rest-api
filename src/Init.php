<?php

namespace WordPressRestApi;

use WordPressPluginAPI\Manager;

class Init
{
    private $hooks;

    public function __construct()
    {
        $this->hooks = [
            new Admin\OptionsPages(),
            new Endpoints\Forms(),
            new Endpoints\Search(),
            new Fields\Editor(),
            new Fields\TaxonomyName(),
            new Fields\Types(),
            new Modify\Pages(),
            new Modify\Posts(),
            new Modify\Users(),
        ];

        // Run PluginAPI registration
        $manager = new Manager();
        foreach ($this->hooks as $class) {
            $manager->register($class);
        }
    }
}
