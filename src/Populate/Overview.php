<?php

namespace WordPressRestApi\Populate;

use WP_Query;
use WP_Term;
use WP_Term_Query;
use WordPressRestApi\Helpers\Populate\Post;
use WordPressRestApi\Helpers\Algolia\Access;

class Overview
{
    private $postId;

    private $postType;

    /**
     * Populate overview
     *
     * @param array           $block   Overview block
     * @param int             $postId  ID of the current post
     *
     * @param array Populated overview block
     */
    public function populateOverview(
        array $block,
        int $postId,
    ): array {
        $accessHelper = new Access();
        $block['attrs']['algoliaSearchKey'] = $accessHelper->getSearchKey();
        $block['attrs']['algoliaApplicationId'] =
            $accessHelper->getApplicationId();

        $this->postId = $postId;
        $this->postType = array_key_exists('postType', $block['attrs']) ?
            array_map(function (array $postType) {
                return $postType['slug'];
            }, $block['attrs']['postType']) :
            'post';

        if (
            array_key_exists('categorizationTaxonomy', $block['attrs']) &&
            array_key_exists('categorizeOverview', $block['attrs']) &&
            $block['attrs']['categorizeOverview']
        ) {
            $initialData = [];
            $initialTotalPosts = 0;

            if ($block['attrs']['categorizationTaxonomy'] !== 'none') {
                $categorizationTaxonomy =
                    $block['attrs']['categorizationTaxonomy'];

                $taxonomyName = get_taxonomy($categorizationTaxonomy)
                    ->labels
                    ->name;
                $terms = get_terms([
                    'taxonomy' => $categorizationTaxonomy,
                ]);

                foreach ($terms as $term) {
                    $query = $this->getQueryCategorizedOverview(
                        $block['attrs'],
                        $term
                    );

                    if ($query->found_posts <= 0) continue;

                    $initialData[] = [
                        'posts' => $this->getData($query),
                        'term' => [
                            'id' => $term->term_id,
                            'name' => $term->name,
                            'slug' => $term->slug,
                            'taxonomyName' => $taxonomyName,
                        ]
                    ];

                    $initialTotalPosts += $query->found_posts;
                }
            }

            $block['attrs']['initialData'] = $this->sortCategorizedPosts(
                $block['attrs']['categorizedOrder'] ?? 'menu',
                $initialData
            );
            $block['attrs']['initialTotalPosts'] = $initialTotalPosts;
            $block['attrs']['taxonomy'] = $categorizationTaxonomy;
            $block['attrs'][''] = $initialTotalPosts;
        } else {
            $query = $this->getQuery($block['attrs']);
            $block['attrs']['initialData'] = $this->getData($query);
            $block['attrs']['initialTotalPosts'] = $query->found_posts;
        }

        if (array_key_exists('postType', $block['attrs'])) {
            $postTypes = array_map(function ($postType) {
                $object = get_post_type_object($postType['slug']);
                $postType['name'] = $object->label;

                return $postType;
            }, $block['attrs']['postType']);

            $block['attrs']['postType'] = $postTypes;
        }

        if (array_key_exists('innerBlocks', $block)) {
            $initialTerms = [];

            if (
                array_key_exists('taxonomy', $block['attrs']) &&
                array_key_exists('terms', $block['attrs']) &&
                $block['attrs']['taxonomy'] !== 'none'
            ) {
                $initialTerms = [
                    'slug' => $block['attrs']['taxonomy'],
                    'terms' => $block['attrs']['terms'],
                ];
            }

            $block['attrs']['initialFilterData'] = $this->getFilterData(
                $block,
                $initialTerms
            );
        }

        if (
            array_key_exists('taxonomy', $block['attrs']) &&
            array_key_exists('terms', $block['attrs'])
        ) {
            $termNames = array_map(function ($termId) use ($block) {
                $term = get_term_by('id', $termId, $block['attrs']['taxonomy']);

                return $term ? $term->name : '';
            }, $block['attrs']['terms']);

            $block['attrs']['terms'] = $termNames;
        }

        return $block;
    }

    private function getQuery(array $attrs): WP_Query
    {
        $showAllPosts = is_array($this->postType) &&
            count($this->postType) === 1 &&
            ($this->postType[0] === 'question' ||
                $this->postType[0] === 'definition'
            ) &&
            (array_key_exists('perPage', $attrs) && $attrs['perPage'] === 100);
        $perPage = array_key_exists('perPage', $attrs) ?
            $attrs['perPage'] :
            12;

        // Arguments object, used to retrieve the initial data
        $args = [
            'post_status' => 'publish',
            'post_type' => $this->postType,
            'post__not_in' => [$this->postId],
            'order' => array_key_exists('order', $attrs) ?
                $attrs['order'] :
                'DESC',
            'orderby' => array_key_exists('orderBy', $attrs) ?
                $attrs['orderBy'] :
                'date',
            'paged' => 1,
            'posts_per_page' => $showAllPosts ? -1 : $perPage,
        ];

        if (
            array_key_exists('taxonomy', $attrs) &&
            array_key_exists('terms', $attrs)
        ) {
            if ($attrs['taxonomy'] !== 'none' && count($attrs['terms']) > 0) {
                $args['tax_query'] = [
                    [
                        'taxonomy' => $attrs['taxonomy'],
                        'field' => 'term_id',
                        'terms' => $attrs['terms'],
                    ]
                ];
            }
        }

        $query = new WP_Query($args);

        return $query;
    }

    /**
     * Get the WordPress query for the categorized overview
     *
     * @param array $attrs Block attributes
     * @param WP_Term $term Term
     *
     * @return WP_Query Initial data
     */
    private function getQueryCategorizedOverview(
        array $attrs,
        WP_Term $term
    ): WP_Query {
        $args = [
            'post_type' => $this->postType,
            'order' => array_key_exists('order', $attrs) ?
                $attrs['order'] :
                'DESC',
            'orderby' => array_key_exists('orderBy', $attrs) ?
                $attrs['orderBy'] :
                'date',
            'posts_per_page' => -1,
            'tax_query' => [
                [
                    'taxonomy' => $attrs['categorizationTaxonomy'],
                    'field' => 'slug',
                    'terms' => $term->slug,
                ]
            ]
        ];

        return new WP_Query($args);
    }

    /**
     * Get the initial post data
     *
     * @param WP_Query $query WordPress query
     *
     * @return array Initial data, based on search params
     */
    private function getData(WP_Query $query): array
    {
        $postData = [];

        if ($query->have_posts()) {
            foreach ($query->posts as $post) {
                $postHelper = new Post($post);
                $filteredPost = $postHelper->filterPost();

                if ($filteredPost !== null) {
                    $postData[] = $filteredPost;
                }
            }
        }

        return $postData;
    }

    /**
     * Get the initial filter data
     *
     * @param array $taxonomies Array of taxonomy slugs
     *
     * @return array Array of filter objects
     */
    private function getFilterData(array $block, array $initialTerms): array
    {
        $filterData = [];

        foreach ($block['innerBlocks'] as $innerBlock) {
            $filterData = array_merge(
                $filterData,
                $this->getFilterDataForBlock($innerBlock, $initialTerms)
            );
        }

        return $filterData;
    }

    private function getFilterDataForBlock(
        array $innerBlock,
        array $initialTerms
    ): array {
        $filterData = [];

        if (array_key_exists('filterSlug', $innerBlock['attrs'])) {
            $taxonomySlug = $innerBlock['attrs']['filterSlug'];
            $taxonomy = get_taxonomy($taxonomySlug);

            if ($taxonomy) {
                $order = array_key_exists('order', $innerBlock['attrs']) ?
                    $innerBlock['attrs']['order'] :
                    'count';

                $filterData['terms.' . $taxonomySlug . '.names'] =
                    $this->getTaxonomyTerms(
                        $taxonomySlug,
                        $order,
                        $initialTerms
                    );
            }
        } elseif (
            array_key_exists('innerBlocks', $innerBlock) &&
            count($innerBlock['innerBlocks']) > 0
        ) {
            $innerFilterData = $this->getFilterData($innerBlock, $initialTerms);

            if (count($innerFilterData) > 0) {
                $filterData = array_merge($filterData, $innerFilterData);
            }
        }

        return $filterData;
    }

    /**
     * Get the taxonomy terms
     *
     * @param string $taxonomySlug Taxonomy slug
     *
     * @return array Array of term objects
     */
    private function getTaxonomyTerms(
        string $taxonomySlug,
        string $order,
        array $initialTerms,
    ): array {
        $query = new WP_Term_Query(
            [
                'taxonomy' => $taxonomySlug,
                'fields' => 'all',
                'exclude' => 1,
                'parent' => 0,
            ]
        );
        $terms = $query->get_terms();
        $termsWithCount = [];

        if (is_array($terms) && count($terms) > 0) {
            foreach ($terms as $term) {
                $taxQuery = [];

                if ($initialTerms && array_key_exists('slug', $initialTerms)) {
                    if ($taxonomySlug === $initialTerms['slug']) {
                        $taxQuery = [
                            [
                                'taxonomy' => $taxonomySlug,
                                'terms' => in_array(
                                    $term->term_id,
                                    $initialTerms['terms']
                                ) ?
                                    $initialTerms['terms'] :
                                    array_merge(
                                        [$term->term_id],
                                        $initialTerms['terms']
                                    ),
                            ]
                        ];
                    } else {
                        $taxQuery = [
                            'relation' => 'AND',
                            [
                                'taxonomy' => $initialTerms['slug'],
                                'terms' => $initialTerms['terms'],
                            ],
                            [
                                'taxonomy' => $taxonomySlug,
                                'terms' => $term->term_id,
                            ],
                        ];
                    }
                } else {
                    $taxQuery = [
                        [
                            'taxonomy' => $taxonomySlug,
                            'terms' => $term->term_id,
                        ],
                    ];
                }

                $args = [
                    'post_type' => $this->postType,
                    'fields' => 'ids',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'tax_query' => $taxQuery,
                ];

                $termQuery = new WP_Query($args);

                if ($termQuery->found_posts > 0) {
                    $termName = htmlspecialchars_decode($term->name);
                    $termsWithCount[$termName] = $termQuery->found_posts;
                }
            }
        }

        if ($order === 'count') {
            arsort($termsWithCount);
        } else if ($order === 'name') {
            ksort($termsWithCount);
        }

        return $termsWithCount;
    }

    /**
     * Sort the categorized overview based on the terms
     *
     * @param string $order Order
     * @param array $posts Posts
     *
     * @return array Sorted array of post objects
     */
    private function sortCategorizedPosts(string $order, array $posts): array
    {
        switch ($order) {
            case "name":
                usort($posts, function ($a, $b) {
                    return strcmp($a['term']['name'], $b['term']['name']);
                });
                return $posts;

            case "count":
                usort($posts, function ($a, $b) {
                    return count($b['posts']) - count($a['posts']);
                });
                return $posts;

            default:
                return $posts;
        }
    }
}
