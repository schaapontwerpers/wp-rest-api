<?php

namespace WordPressRestApi\Populate;

use WordPressRestApi\Helpers\Populate\Post;

class Editorial
{
    /**
     * Populate editorial
     *
     * @param array $block Editorial block
     *
     * @param array Populated editorial block
     */
    public function populateEditorial(array $block): array
    {
        if (array_key_exists('postId', $block['attrs'])) {
            $post = get_post($block['attrs']['postId']);

            if ($post) {
                $postHelper = new Post($post);

                $block['attrs']['post'] = $postHelper->filterPost();
            }
        }

        return $block;
    }
}
