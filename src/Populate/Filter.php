<?php

namespace WordPressRestApi\Populate;

class Filter
{
    /**
     * Populate filter
     *
     * @param array $block Filter block
     *
     * @param array Populated filter block
     */
    public function populateFilter(array $block): array
    {
        if (array_key_exists('attrs', $block)) {
            if (array_key_exists('filterSlug', $block['attrs'])) {
                $taxonomy = get_taxonomy($block['attrs']['filterSlug']);

                if ($taxonomy) {
                    $block['attrs']['filterName'] = $taxonomy->label;
                }
            }
        }

        return $block;
    }
}
