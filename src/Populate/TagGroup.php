<?php

namespace WordPressRestApi\Populate;

use WordPressRestApi\Helpers\Populate\Post;
use WP_Query;

/**
 * Class TagGroup
 *
 * @package WordPressRestApi\Populate
 */
class TagGroup
{

    private $filterBlock;

    private $searchBlock;

    /**
     * Populate tag-group
     *
     * @param array $block  TagGroup block
     * @param int   $postId ID of the post
     *
     * @return array Populated tag-group block
     */
    public function populateTagGroup(array $block, int $postId): array
    {
        $postHelper = new Post(get_post($postId));

        $selectedTaxonomy = array_key_exists(
            'selectedTaxonomy',
            $block['attrs']
        ) ?
            $block['attrs']['selectedTaxonomy'] :
            'none';
        $terms = $selectedTaxonomy === 'none' ?
            [] :
            get_the_terms($postId, $selectedTaxonomy);

        if ($postHelper && is_array($terms)) {
            $formattedTerms = $this->formatTerms($terms, $postHelper);

            $block['attrs']['terms'] = $formattedTerms;
            $block['attrs']['postType'] = $postHelper->getPostType();
        }

        return $block;
    }

    /**
     * Format terms
     *
     * @param array $terms
     * @param Post $postHelper
     *
     * @return array
     */
    private function formatTerms(array $terms, Post $postHelper): array
    {
        $formattedTerms = [];

        foreach ($terms as $term) {
            if (isset($term->taxonomy)) {
                $taxonomy = $term->taxonomy;

                $formattedTerm = (array) $term;
                $formattedTerm['url'] = $this->getFallbackUrl(
                    $postHelper->getPostType()['slug'],
                    $term->slug,
                    $taxonomy
                );

                $formattedTerms[] = $formattedTerm;
            }
        }

        return $formattedTerms;
    }


    /**
     * Get fallback URL
     *
     * @param string $postType
     * @param string $termSlug
     * @param string $taxonomy
     *
     * @return string
     */
    private function getFallbackUrl(
        string $postType,
        string $termSlug,
        string $taxonomy
    ): string {
        $frontendUrl = function_exists('get_field') ?
            get_field('frontend_url', 'options') :
            null;

        $overviewUrls = acf_get_fields('group_overview_pages');
        $searchResults = get_field('search_results', 'options') ?
            basename(untrailingslashit(
                str_replace(
                    $frontendUrl,
                    '',
                    get_field('search_results', 'options')
                )
            )) :
            '';

        $pageUrl = '';
        $fallbackUrl = '';

        foreach ($overviewUrls as $field) {
            $hasOverviewPage = $field['name'] === $postType;

            if (
                $hasOverviewPage &&
                !empty(get_field($field['name'], 'options'))
            ) {
                $pageUrl = get_field($field['name'], 'options');

                break;
            }
        }

        $pageUrl = basename(untrailingslashit(
            str_replace($frontendUrl, '', $pageUrl)
        ));
        $filterPath = '/' . $pageUrl . '?';

        $this->hasTermFilterInnerBlock($pageUrl, $taxonomy);

        if ($this->filterBlock && $hasOverviewPage) {
            $fallbackUrl =  $filterPath . $taxonomy . '=' . $termSlug;
        } else if (
            $this->searchBlock &&
            $hasOverviewPage &&
            !$this->filterBlock
        ) {
            $fallbackUrl =  $filterPath . 's=' . $termSlug;
        } else {
            $fallbackUrl =  '/' . $searchResults . '?' . 's=' . $termSlug;
        }

        return $fallbackUrl;
    }

    /**
     * Check if a specific block exists within the blocks
     *
     * @param array $blocks
     * @param string $blockName
     * @param string|null $taxonomy
     *
     * @return array|null
     */
    private function findBlock(
        array $blocks,
        string $blockName,
        ?string $taxonomy = null
    ): ?array {
        $blockFound = null;

        foreach ($blocks as $block) {
            if ($block['blockName'] === $blockName) {
                // If a specific taxonomy is provided, check if the current block matches that taxonomy
                if (
                    $taxonomy &&
                    isset($block['attrs']['filterSlug']) &&
                    $block['attrs']['filterSlug'] === $taxonomy
                ) {
                    // Set flag to true since the block is found
                    $blockFound = $block;

                    break;
                } elseif (!$taxonomy) {
                    // If no specific taxonomy is provided, return the block
                    $blockFound = $block;

                    break;
                }
            } elseif (
                array_key_exists('innerBlocks', $block) &&
                count($block['innerBlocks']) > 0
            ) {
                // Recursively search for the block within the innerBlocks of the current block
                $foundBlock = $this->findBlock(
                    $block['innerBlocks'],
                    $blockName,
                    $taxonomy
                );

                if ($foundBlock) {
                    $blockFound = $foundBlock;

                    break;
                }
            }
        }

        // No block found that meets the criteria
        return $blockFound;
    }

    /**
     * Check if the term filter inner block exists
     *
     * @param string $path
     * @param string $taxonomy
     */
    private function hasTermFilterInnerBlock(string $path, string $taxonomy)
    {
        $pagesPostType = 'page';

        $query = new WP_Query([
            'posts_per_page' => 1,
            'post_type' => $pagesPostType,
            'name' => $path,
        ]);

        if ($query->have_posts()) {
            $content = $query->posts[0]->post_content;
            $blocks = parse_blocks($content);

            $this->filterBlock = $this->findBlock(
                $blocks,
                'jabbado/filter',
                $taxonomy
            );

            $this->searchBlock = $this->findBlock(
                $blocks,
                'jabbado/overview-search'
            );
        }
    }
}
