<?php

namespace WordPressRestApi\Fields;

use WP_REST_Request;
use WP_REST_Response;
use WP_Post;
use WordPressClassHelpers\RestAPI\CustomFields;

class Types extends CustomFields
{
    /**
     * Register Gutenberg blocks as API field
     */
    public function registerFields()
    {
        register_rest_field(
            'type',
            'url_base',
            [
                'get_callback' => function (array $post) {
                    return $this->getUrlBase($post['slug']);
                },
                'schema' => [
                    'description' => __(
                        'URL base for routing of a post type.',
                        'jabbado'
                    ),
                    'type' => 'string',
                ]
            ]
        );
    }

    /**
     * Register Gutenberg blocks as API field for preview
     */
    public function registerPreviewFields(
        WP_REST_Response $response,
        WP_Post $post,
        WP_Rest_Request $request,
    ): WP_REST_Response {
        $data = $response->get_data();

        // Add general API data if correct post type
        $data['url_base'] = $this->getUrlBase($post->slug);

        $response->set_data($data);
        return $response;
    }

    /**
     * Get the URL base for each type
     */
    private function getUrlBase(string $postType): string
    {
        $urlBase = $postType;

        if ($postType === 'post') {
            $structure = get_option('permalink_structure');
            $urlBase = trim(str_replace('%postname%', '', $structure), '/');
        } else {
            $postTypeObject = get_post_type_object($postType);

            if (
                $postTypeObject &&
                is_array($postTypeObject->rewrite) &&
                array_key_exists('slug', $postTypeObject->rewrite)
            ) {
                $urlBase = $postTypeObject->rewrite['slug'];
            }
        }

        return $urlBase;
    }
}
