<?php

namespace WordPressRestApi\Fields;

use WP_REST_Request;
use WP_REST_Response;
use WP_Post;
use WP_Query;
use WordPressRestApi\Helpers\Populate\Term;
use WordPressClassHelpers\RestAPI\CustomFields;

class TaxonomyName extends CustomFields
{
    /**
     * ID of the current post
     */
    private $slug;

    /**
     * ID of the current post
     */
    private $taxonomy;

    /**
     * ID of the current post
     */
    private $request;

    /**
     * Register Gutenberg blocks as API field
     */
    public function registerFields()
    {
        $taxonomies = get_taxonomies([], 'names');

        foreach ($taxonomies as $taxonomy) {
            register_rest_field(
                $taxonomy,
                'taxonomy_name',
                [
                    'get_callback' => function (
                        array $post,
                        string $attr,
                        WP_REST_Request $request,
                    ) {
                        $taxonomyName = '';

                        if (
                            array_key_exists('slug', $post) &&
                            array_key_exists('taxonomy', $post)
                        ) {
                            $this->taxonomy = $post['taxonomy'];
                            $this->request = $request;

                            $taxonomyName = $this->getTaxonomyName();
                        }

                        return $taxonomyName;
                    },
                    'schema' => [
                        'description' => __(
                            'Name of the taxonomy.',
                            'jabbado'
                        ),
                        'type' => 'string',
                    ]
                ]
            );
        }
    }

    /**
     * Register Gutenberg blocks as API field for preview
     */
    public function registerPreviewFields(
        WP_REST_Response $response,
        WP_Post $post,
        WP_Rest_Request $request,
    ): WP_REST_Response {
        return $response;
    }

    /**
     * Get the blocks field
     */
    private function getTaxonomyName(): string
    {
        $taxonomy = get_taxonomy($this->taxonomy);

        return $taxonomy ? $taxonomy->label : '';
    }
}
