<?php

namespace WordPressRestApi\Fields;

use WP_REST_Request;
use WP_REST_Response;
use WP_Post;
use WordPressRestApi\Helpers\Fields\Blocks;
use WordPressClassHelpers\RestAPI\CustomFields;

class Editor extends CustomFields
{
    /**
     * ID of the current post
     */
    private $postId;

    /**
     * ID of the current post
     */
    private $postType;

    /**
     * Register Gutenberg blocks as API field
     */
    public function registerFields()
    {
        $postTypes = get_post_types_by_support(['editor']);

        foreach ($postTypes as $postType) {
            register_rest_field(
                $postType,
                'clean_blocks',
                [
                    'get_callback' => function (array $post) {
                        $blocks = [];

                        if (array_key_exists('id', $post)) {
                            $this->postId = $post['id'];
                            $this->postType = get_post_type($this->postId);

                            $blocks = $this->getBlocks();
                        }

                        return $blocks;
                    },
                    'schema' => [
                        'description' => __(
                            'Filtered Gutenberg blocks for a post, with populated data.',
                            'jabbado'
                        ),
                        'type' => 'array',
                        'items' => [
                            'type' => 'object',
                            'properties' => [
                                'blockName' => [
                                    'type' => 'string',
                                ],
                                'attrs' => [
                                    'type' => 'object'
                                ],
                                'innerBlocks' => [
                                    'type' => 'array'
                                ],
                                'id' => [
                                    'type' => 'string'
                                ]
                            ]
                        ]
                    ]
                ]
            );

            register_rest_field(
                $postType,
                'content_blocks',
                [
                    'get_callback' => function (array $post) {
                        $blocks = [];

                        if (array_key_exists('id', $post)) {
                            $this->postId = $post['id'];
                            $this->postType = get_post_type($this->postId);

                            $blocks = $this->getContentBlocks();
                        }

                        return $blocks;
                    },
                    'schema' => [
                        'description' => __(
                            'Filtered out Gutenberg blocks for a private post, with populated data.',
                            'jabbado'
                        ),
                        'type' => 'array',
                        'items' => [
                            'type' => 'object',
                            'properties' => [
                                'blockName' => [
                                    'type' => 'string',
                                ],
                                'attrs' => [
                                    'type' => 'object'
                                ],
                                'innerBlocks' => [
                                    'type' => 'array'
                                ],
                                'id' => [
                                    'type' => 'string'
                                ]
                            ]
                        ]
                    ]
                ]
            );
        }
    }

    /**
     * Register Gutenberg blocks as API field for preview
     */
    public function registerPreviewFields(
        WP_REST_Response $response,
        WP_Post $post,
        WP_Rest_Request $request,
    ): WP_REST_Response {
        $data = $response->get_data();

        // Original post ID
        $this->postId = $data['parent'];
        $this->postType = get_post_type($this->postId);

        // Post types
        $postTypes = get_post_types_by_support(['editor']);

        // Add general API data if correct post type
        if (in_array($this->postType, $postTypes)) {
            $data['clean_blocks'] = $this->parseBlocks($data['content']['raw']);
        }

        $response->set_data($data);
        return $response;
    }

    /**
     * Get the blocks field
     */
    private function getBlocks(): array
    {
        $content = get_post_field('post_content', $this->postId);

        return $this->parseBlocks($content);
    }

    private function getContentBlocks(): array
    {
        $contentBlocks = [];

        $content = get_post_field('post_content', $this->postId);
        $parseBlocks = parse_blocks($content);

        if (
            is_array($parseBlocks) &&
            count($parseBlocks) > 0 &&
            $parseBlocks[0]['blockName'] === 'jabbado/content' &&
            array_key_exists('innerBlocks', $parseBlocks[0])
        ) {
            $blocks = new Blocks(
                $this->postId,
                $this->postType,
                $parseBlocks[0]['innerBlocks'],
            );
            $blocks->clean();

            $contentBlocks = $blocks->blocks;
        }

        return $contentBlocks;
    }

    /**
     * Get the blocks field
     */
    private function parseBlocks(string $content): array
    {
        $parseBlocks = parse_blocks($content);

        $blocks = new Blocks(
            $this->postId,
            $this->postType,
            $parseBlocks,
        );
        $blocks->clean();

        return $blocks->blocks;
    }
}
