<?php

namespace WordPressRestApi\Admin;

use WordPressPluginAPI\ActionHook;
use WordPressPluginAPI\FilterHook;

class OptionsPages implements ActionHook, FilterHook
{
    /**
     * Subscribe functions to corresponding actions
     */
    public static function getActions(): array
    {
        return [
            'acf/init' => 'addPages',
        ];
    }

    /**
     * Subscribe functions to corresponding actions
     */
    public static function getFilters(): array
    {
        return [
            'acf/settings/load_json' => 'addPluginJson',
        ];
    }

    public function addPluginJson(array $paths): array
    {
        // Append the new path and return it.
        $paths[] = WPMU_PLUGIN_DIR . '/sdc-wp-rest-api/acf-json';

        return $paths;
    }

    /**
     * Add options pages
     */
    public function addPages()
    {
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page(
                [
                    'page_title'  => __('Overview', 'jabbado'),
                    'menu_slug'   => 'overviews',
                    'parent_slug' => 'options-general.php',
                    'capability'  => 'create_users',
                ]
            );

            acf_add_options_page(
                [
                    'page_title'  => __('Platform', 'jabbado'),
                    'menu_slug'   => 'platform',
                    'parent_slug' => 'options-general.php',
                    'capability'  => 'remove_users',
                ]
            );

            $this->addOverviewFields();
        }
    }

    private function addOverviewFields()
    {
        // Get all public post types
        $postTypes = get_post_types(['public' => true], 'objects');

        // Location for the field group
        $location = [
            'location' => [
                [
                    [
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'overviews',
                    ],
                ],
            ],
        ];

        // Array to store all field definitions within a single field group
        $fields = [
            [
                'key' => 'field_overview_search_results',
                'label' => 'Search Results',
                'name' => 'search_results',
                'type' => 'page_link',
                'post_type' => ['page'],
                'allow_null' => true,
            ]
        ];

        // Add field definitions for the overview pages of each post type
        foreach ($postTypes as $postType => $postTypeObject) {
            if (!in_array($postType, ['attachment', 'page'])) {
                $fields[] = [
                    'key' => 'field_overview_' . $postType,
                    'label' => $postTypeObject->label,
                    'name' => $postType,
                    'type' => 'page_link',
                    'post_type' => ['page'],
                    'allow_null' => true,
                ];
            }
        }

        // Add a single field group containing all fields
        acf_add_local_field_group([
            'key' => 'group_overview_pages',
            'title' => 'Overview Pages',
            'fields' => $fields,
            'location' => $location['location'],
        ]);
    }
}
