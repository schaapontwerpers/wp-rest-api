<?php

namespace WordPressRestApi\Modify;

use WP_REST_Request;
use WordPressPluginAPI\FilterHook;
use WordPressRestApi\Helpers\Modify\Params;

class Posts implements FilterHook
{
    /**
     * The post type name
     */
    private static $postType = 'post';

    /**
     * Subscribe functions to corresponding filters
     */
    public static function getFilters(): array
    {
        $postType = static::$postType;

        return [
            'rest_' . $postType . '_query' => ['editQuery', 10, 2],
        ];
    }

    /**
     * Add parent slug to arguments for better response
     */
    public function editQuery(
        array $args,
        WP_REST_Request $request,
    ): array {
        // Get all parameters
        $params = new Params($request);
        $params->setArgs($args);
        $params->setPostType();
        $params->setOrderQuery();

        return $params->args;
    }
}
