<?php

namespace WordPressRestApi\Modify;

use WordPressPluginAPI\FilterHook;

class Users implements FilterHook
{
    /**
     * Subscribe functions to corresponding filters
     */
    public static function getFilters(): array
    {
        $filters = [
            'rest_user_query' => [
                'removePostsArg',
                10,
                2,
            ]
        ];

        return $filters;
    }

    /**
     * Remove published posts arg to show all users on request
     */
    public function removePostsArg(array $args): array
    {
        unset($args['has_published_posts']);

        return $args;
    }
}
