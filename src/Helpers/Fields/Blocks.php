<?php

namespace WordPressRestApi\Helpers\Fields;

use WordPressRestApi\Populate\Editorial;
use WordPressRestApi\Populate\Filter;
use WordPressRestApi\Populate\Overview;
use WordPressRestApi\Populate\TagGroup;

class Blocks
{
    public $blocks;

    private $postId;

    public function __construct(
        int $postId,
        string $postType,
        array $blocks,
    ) {
        $this->blocks = $this->convertBlocks($blocks);
        $this->postId = $postId;
    }

    /**
     * Convert reusable blocks to the right block data
     */
    private function convertBlocks(array $blocks): array
    {
        $convertedBlocks = [];

        foreach ($blocks as $block) {
            $convertedBlocks = array_merge(
                $convertedBlocks,
                $this->convertBlock($block)
            );
        }

        return $convertedBlocks;
    }

    /**
     * Convert a single reusable block to the right block data
     */
    private function convertBlock(array $block): array
    {
        $convertedBlocks = [];

        if ($block['blockName'] === 'core/block') {
            $blockId = $block['attrs']['ref'];
            $post = get_post($blockId);

            if ($post) {
                $parsedBlocks = parse_blocks($post->post_content);

                // Clean all innerBlocks aswell
                foreach ($parsedBlocks as $parsedBlock) {
                    $convertedBlock = $this->convertBlock($parsedBlock);

                    if (array_key_exists('innerBlocks', $convertedBlock)) {
                        $innerBlocks = [];

                        foreach ($convertedBlock['innerBlocks'] as $innerBlock) {
                            $innerBlocks = array_merge(
                                $innerBlocks,
                                $this->convertBlock($innerBlock),
                            );
                        }

                        $convertedBlock['innerBlocks'] = $innerBlocks;
                    }

                    $convertedBlocks = array_merge($convertedBlocks, $convertedBlock);
                }
            }
        } else {
            $convertedBlock = $block;

            if (array_key_exists('innerBlocks', $convertedBlock)) {
                $innerBlocks = [];

                foreach ($convertedBlock['innerBlocks'] as $innerBlock) {
                    $innerBlocks = array_merge(
                        $innerBlocks,
                        $this->convertBlock($innerBlock)
                    );
                }

                $convertedBlock['innerBlocks'] = $innerBlocks;
            }

            array_push($convertedBlocks, $convertedBlock);
        }

        return $convertedBlocks;
    }

    public function clean(): void
    {
        $cleanBlocks = [];

        foreach ($this->blocks as $key => $parseBlock) {
            if ($parseBlock['blockName'] !== null) {
                $cleanBlocks[] = $this->cleanBlock($parseBlock);
            }
        }

        $this->blocks = $cleanBlocks;
    }

    private function cleanBlock(array $block): array
    {
        // Add additional data
        $block = $this->addData($block);
        $block['id'] = wp_unique_id('block-');

        unset($block['innerHTML']);
        unset($block['innerContent']);

        // Check if block has it's own blocks inside
        if (array_key_exists('innerBlocks', $block) && count($block['innerBlocks']) > 0) {
            $innerBlocks = [];

            // Clean all innerBlocks aswell
            foreach ($block['innerBlocks'] as $innerBlock) {
                $innerBlocks[] = $this->cleanBlock($innerBlock);
            }

            $block['innerBlocks'] = $innerBlocks;
        } else {
            unset($block['innerBlocks']);
        }

        // Always return attributes as array
        if (!$block['attrs']) {
            $block['attrs'] = (object) array();
        }

        return $block;
    }

    private function addData(array $block): array
    {
        $editorial = new Editorial();
        $filter = new Filter();
        $overview = new Overview();
        $tagGroup = new TagGroup();

        switch ($block['blockName']) {
            case 'jabbado/overview':
                $block = $overview->populateOverview(
                    $block,
                    $this->postId,
                );

                break;

            case 'jabbado/filter':
                $block = $filter->populateFilter($block);

                break;

            case 'jabbado/editorial':
                $block = $editorial->populateEditorial($block);

                break;

            case 'jabbado/tag-group':
                $block = $tagGroup->populateTagGroup($block, $this->postId);

                break;

            case 'jabbado/paragraph':
            case 'jabbado/heading':
            case 'jabbado/list-item':
                $block = $this->addContent($block);

                break;

            default:
                break;
        }

        return apply_filters(
            'jabbado_populate_block',
            $block,
            $this->postId
        );
    }

    private function addContent(array $block): array
    {
        if (!array_key_exists('content', $block['attrs'])) {
            $block['attrs']['content'] = trim(strip_tags(
                html_entity_decode($block['innerHTML']),
                '<a><mark><i><strong><b><br><em><span><u><sub><sup><s>'
            ));
        }

        return $block;
    }
}
