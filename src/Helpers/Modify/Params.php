<?php

namespace WordPressRestApi\Helpers\Modify;

use WP_REST_Request;

class Params
{
    public $args = [];

    private $request;

    public function __construct(WP_REST_Request $request)
    {
        $this->request = $request;
    }

    public function getParam($param)
    {
        return $this->request->has_param($param) ?
            $this->request->get_param($param) :
            null;
    }

    public function setArgs(array $args)
    {
        $this->args = $args;
    }

    public function setArg(string $arg, $value)
    {
        $this->args[$arg] = $value;
    }

    /**
     * Add parent to WP Query arguments
     */
    public function setPostParent($type)
    {
        $param = $this->getParam('parentSlug');

        if ($param) {
            $page = get_page_by_path(
                $param,
                OBJECT,
                $type
            );

            if ($page) {
                $this->args['post_parent'] = $page->ID;
            }
        }
    }

    /**
     * Set correct orderby. Examples are "title", "relevance" and "date".
     *
     * @return string
     */
    private function setOrderBy(): string
    {
        $orderBy = $this->getParam('orderby');

        return $orderBy ?: 'relevance';
    }

    /**
     * Set correct order. Options are "ascending" or "descending".
     *
     * @return string
     */
    private function setOrder(): string
    {
        $orderParam = $this->getParam('order');
        $orderByParam = $this->getParam('orderby');

        if ($orderParam) {
            $order = strtoupper($orderParam);
        } else if (!$orderParam && $orderByParam && $orderByParam === 'title') {
            // Ascending title is A-Z while ascending date is from Old - New, so therefore
            // we have to switch it around
            $order = 'ASC';
        } else {
            $order = 'DESC';
        }

        return $order;
    }

    /**
     * Set correct order
     *
     * @return void
     */
    public function setOrderQuery()
    {
        $sortingParams = $this->getSortingParams();

        $this->args['orderby'] = $sortingParams['orderby'];
        $this->args['order'] = $sortingParams['order'];
    }

    /**
     * Sets the sorting params for orderby and order
     *
     * @return void
     */
    public function getSortingParams(): array
    {
        return [
            'orderby' => $this->setOrderBy(),
            'order' => $this->setOrder()
        ];
    }

    /**
     * Add post type to WP Query arguments
     */
    public function setPostType()
    {
        $param = $this->getParam('postType');

        if ($param) {
            $explodedPostType = explode(",", $param);
            $postTypes = [];

            foreach ($explodedPostType as $label) {
                if (post_type_exists($label)) {
                    $postTypes[] = $label;
                } else {
                    $postType = get_post_types(['label' => $label]);

                    if ($postType) {
                        $postTypes[] = array_values($postType)[0];
                    }
                }
            }

            if (count($postTypes) > 0) {
                $this->args['post_type'] = $postTypes;
            }
        }
    }

    /**
     * Add post type to WP Query arguments
     */
    public function setTaxQuery()
    {
        $taxonomies = get_taxonomies([], "objects");
        $taxQuery = [];

        foreach ($taxonomies as $taxonomy) {
            $restBase = $taxonomy->name;
            $param = $this->getParam($restBase);

            if ($param) {
                $taxQuery[] = [
                    'taxonomy' => $taxonomy->name,
                    'field' => 'name',
                    'terms' => explode(',', $param),
                ];
            }
        }

        if (count($taxQuery) > 0) {
            if (count($taxQuery) > 1) {
                $taxQuery['relation'] = 'AND';
            }
            $this->args['tax_query'] = $taxQuery;
        }
    }
}
