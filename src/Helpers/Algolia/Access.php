<?php

namespace WordPressRestApi\Helpers\Algolia;

use Algolia\AlgoliaSearch\SearchClient;
use Groups_User;

class Access
{
    private $headers;

    public function __construct()
    {
        $this->headers = getallheaders();
    }

    public function getApplicationId(): string
    {
        return get_field('algolia_application_id', 'options') ?: '';
    }

    public function getSearchKey(): string
    {
        $currentUser = !array_key_exists('Authorization', $this->headers) ?
            0 :
            get_current_user_id();
        $userGroups = $this->getUserGroups($currentUser);
        $key = $this->generateAPIKey($userGroups);

        return $key;
    }

    public function generateAPIKey(array $userGroups): string
    {
        $filters = [];

        foreach ($userGroups as $userGroup) {
            $filters[] = "visible_by:{$userGroup}";
        }

        $filtersString = implode(' OR ', $filters);

        $apiKey = get_field('algolia_api_key', 'options');
        $publicKey = $apiKey ?
            SearchClient::generateSecuredApiKey(
                $apiKey,
                [
                    'filters' => $filtersString
                ]
            ) :
            '';

        return $publicKey ?: '';
    }

    private function getUserGroups(int $userId): array
    {
        if (class_exists('Groups_User')) {
            $user = new Groups_User($userId);

            return $userId !== 0 ?
                array_merge(['Everybody'], $user->get_group_ids()) :
                ['Everybody'];
        } else {
            return ["Everybody"];
        }
    }
}
