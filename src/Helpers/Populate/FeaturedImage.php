<?php

namespace WordPressRestApi\Helpers\Populate;

use WP_Post;
use stdClass;

class FeaturedImage
{
    private $post;

    private $postId;

    /**
     * Constructor
     */
    public function __construct(int $postId)
    {
        // Only proceed if the post has a featured image.
        $imageId = get_post_thumbnail_id($postId);

        $this->post = $imageId ? get_post($imageId) : false;
        $this->postId = $imageId;
    }

    public function getObject(): array|false
    {
        return $this->postId ?
            [
                'id' => $this->postId,
                'date' => $this->getDate(),
                'slug' => $this->getSlug(),
                'type' => 'attachment',
                'title' => $this->getTitle(),
                'caption' => $this->getCaption(),
                'alt_text' => $this->getAlt(),
                'media_type' => $this->getMediaType(),
                'mime_type' => $this->getMimeType(),
                'media_details' => $this->getMediaDetails(),
                'source_url' => $this->getSourceUrl(),
            ] :
            false;
    }

    public function getDate(): string
    {
        return mysql_to_rfc3339($this->post->post_date);
    }

    public function getSlug(): string
    {
        return $this->post->post_name;
    }

    public function getTitle(): array
    {
        return [
            'rendered' => apply_filters('the_title', $this->post->post_title),
        ];
    }

    public function getCaption(): array
    {
        $caption = apply_filters(
            'get_the_excerpt',
            $this->post->post_excerpt,
            $this->post
        );

        $caption = apply_filters('the_excerpt', $caption);

        return [
            'rendered' => $caption,
        ];
    }

    public function getAlt(): string
    {
        return get_post_meta($this->postId, '_wp_attachment_image_alt', true);
    }

    public function getMediaType(): string
    {
        return wp_attachment_is_image($this->postId) ? 'image' : 'file';
    }

    public function getMimeType(): string
    {
        return $this->post->post_mime_type;
    }

    public function getMediaDetails(): array|false
    {
        $mediaDetails = wp_get_attachment_metadata($this->postId);

        // Ensure empty details is an empty object.
        if (empty($mediaDetails)) {
            $data['media_details'] = new stdClass();
        } elseif (!empty($mediaDetails['sizes'])) {
            foreach ($mediaDetails['sizes'] as $size => &$sizeData) {
                if (isset($sizeData['mime-type'])) {
                    $sizeData['mime_type'] = $sizeData['mime-type'];
                    unset($sizeData['mime-type']);
                }

                // Use the same method image_downsize() does.
                $imageSrc = wp_get_attachment_image_src($this->postId, $size);
                if (!$imageSrc) {
                    continue;
                }

                $sizeData['source_url'] = $imageSrc[0];
            }

            $fullSrc = wp_get_attachment_image_src($this->postId, 'full');

            if (!empty($fullSrc)) {
                $mediaDetails['sizes']['full'] = array(
                    'file' => wp_basename($fullSrc[0]),
                    'width' => $fullSrc[1],
                    'height' => $fullSrc[2],
                    'mime_type' => $this->post->post_mime_type,
                    'source_url' => $fullSrc[0],
                );
            }
        } else {
            $mediaDetails['sizes'] = new stdClass();
        }

        return $mediaDetails;
    }

    public function getSourceUrl(): string
    {
        return wp_get_attachment_url($this->postId);
    }
}
