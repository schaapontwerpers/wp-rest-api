<?php

namespace WordPressRestApi\Helpers\Populate;

use WP_Post;
use WordPressRestApi\Helpers\Fields\Blocks;

class Post
{
    private $post;

    private $postId;

    /**
     * Constructor
     */
    public function __construct(WP_Post $post)
    {
        $this->post = $post;
        $this->postId = $post->ID;
    }

    /**
     * Filter a post object
     *
     * @return array|null Filtered post object or null if post should be excluded
     */
    public function filterPost(): ?array
    {
        $excludePost = get_field('exclude_post', $this->postId);
        $frontendUrl = get_field('frontend_url', 'options');

        if ($excludePost === true) {
            return null;
        }

        return [
            'objectID' => implode('#', [$this->post->post_type, $this->postId]),
            'type' => $this->getPostType(),
            'slug' => $this->post->post_name,
            'title' => $this->post->post_title,
            'link' => str_replace(
                home_url(),
                $frontendUrl,
                get_permalink($this->post->ID)
            ),
            'date' => strtotime($this->post->post_date),
            'author' => [
                'id' => $this->post->post_author,
                'name' => get_user_by('ID', $this->post->post_author)->display_name,
            ],
            'image' => $this->getImage(),
            'excerpt' => $this->post->post_excerpt,
            'content' => trim(strip_tags($this->post->post_content)),
            'contentBlocks' => $this->getBlocks(),
            'acf' => function_exists('get_fields') ?
                (get_fields($this->postId) ?: []) :
                [],
            'terms' => $this->getTerms(),
        ];
    }

    public function getPostType(): array
    {
        $postType = get_post_type_object($this->post->post_type);

        return [
            'slug' => $this->post->post_type,
            'name' => $postType->label,
        ];
    }

    private function getImage(): array
    {
        // Only proceed if the post has a featured image.
        $imageId = get_post_thumbnail_id($this->postId);
        $imageData = [];

        if ($imageId) {
            $alt = get_post_meta($imageId, '_wp_attachment_image_alt', true);
            $metadata = wp_get_attachment_metadata($imageId);
            $url = wp_get_attachment_url($imageId);

            $imageData = [
                'alt' => $alt,
                'width' => $metadata['width'],
                'height' => $metadata['height'],
                'url' => $url,
            ];
        }

        return $imageData;
    }

    /**
     * Get the post terms from all taxonomies or a single post
     */
    private function getTerms(): array
    {
        $taxonomies = [];
        $postTaxonomies = get_post_taxonomies($this->postId);

        foreach ($postTaxonomies as $postTaxonomy) {
            $taxonomy = get_taxonomy($postTaxonomy);

            $terms = wp_get_post_terms(
                $this->postId,
                $postTaxonomy,
                [
                    'fields' => 'names',
                ],
            );

            $taxonomies[$postTaxonomy] = [
                'label' => $taxonomy->label,
                'names' => $terms,
            ];
        }

        return $taxonomies;
    }

    /**
     * Get the post terms from all taxonomies of a single post
     */
    private function getBlocks(): array|false
    {
        $postType = get_post_type_object($this->post->post_type);

        return !$postType->public ? $this->parseBlocks() : [];
    }

    /**
     * Get the blocks field
     */
    private function parseBlocks(): array
    {
        $contentBlocks = [];

        $content = get_post_field('post_content', $this->postId);
        $parseBlocks = parse_blocks($content);

        if (
            is_array($parseBlocks) &&
            count($parseBlocks) > 0 &&
            $parseBlocks[0]['blockName'] === 'jabbado/content' &&
            array_key_exists('innerBlocks', $parseBlocks[0])
        ) {
            $blocks = new Blocks(
                $this->postId,
                $this->post->postType,
                $parseBlocks[0]['innerBlocks'],
            );
            $blocks->clean();

            $contentBlocks = $blocks->blocks;
        }

        return $contentBlocks;
    }
}
